FROM centos:latest

MAINTAINER Bimlesh

ENV KAFKA_HOME /usr/local/kafka
ADD ./start-kafka.sh /scripts/
#WORKDIR /usr/local/kafka

# install java + others
#RUN yum update 
RUN yum install -y wget 
RUN yum install -y vim
RUN yum install -y java-1.8.0-openjdk
#RUN yum install -y java-1.8.0-openjdk-devel

# install kafka
RUN wget https://www.apache.org/dist/kafka/2.1.1/kafka_2.11-2.1.1.tgz
RUN tar -xvf kafka_2.11-2.1.1.tgz 
RUN mv kafka_2.11-2.1.1 $KAFKA_HOME
RUN chmod a+x /scripts/start-kafka.sh

CMD ["/scripts/start-kafka.sh"]
