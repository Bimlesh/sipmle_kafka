## Simple Kafka Dockerfile
## Kafka
Apache Kafka is a fault tolerant publish-subscribe streaming platform that lets you process streams of records as they occur.
This Dockerfile is very simple. It installs Java and wget, downloads and install Kafka, and starts the Zookeeper and Kafka servers (as part of another script). 

## Pre-Requisite
- Docker installed  
- User with sudo privileges

## Starting Zookeeper and Kafka
In the Dockerfile above you can see CMD ["/scripts/start-kafka.sh"]. This command is used to start both Zookeeper and Kafka servers. 

## Building the Kafka Image
Once we have our Dockerfile and start-kafka.sh files ready, we can build this docker image
```
docker build -t kafka-quickstart .
```
## Starting a Kafka Docker Container
Once the image is built we can create and run a new Kafka Docker container. The command below will create a container from our image and name it kafka-quickstart
```
docker run --name kafka-quickstart -d kafka-quickstart
```
# Testing kafka
After running kafka in one terminal with:
``` 	
docker run --name kafka-quickstart  kafka-quickstart:0.10.2.0
```
## Create a topic "test" in a new SSH connection
Get into the container:
```
docker exec -it kafka-quickstart bash
```
``` 
cd /usr/local/kafka/
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
```
You can view your topics with the following command:
``` 
bin/kafka-topics.sh --list --zookeeper localhost:2181
```
## Produce messages using the topic "test"
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
Using the command above, you can input any number of messages as you wish, such as:
``` 
Welcome aboard!
Bonjour!
```
## Display messages
Get into the container:
```
docker exec -it kafka-quickstart bash
```
``` 
cd /usr/local/kafka/
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
```
Ta-da! The messages you produced earlier will display in the third SSH connection. Of course, if you input more messages from the second SSH connection now, you will immediately see them on the third SSH connection.

# Refrence Link
- https://www.vultr.com/docs/how-to-install-apache-kafka-on-centos-7
- http://bigdatums.net/2017/04/22/apache-kafka-docker-image-example/
